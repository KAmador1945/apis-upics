let util = require('util');
let path = require('path');
let addr = require('ip')
let ip = addr.address();  

module.exports = {

  //update portrait pic
  async UpdatePortrait(req, res) {
    try {

      //upload profile pic
      let UploadPathPic = await sails.uploadOne(req.file('file'), {
        dirname:require('path').resolve(`.tmp/public/images/profile/portrait/`),
        maxBytes:3000000
      }).intercept('E_EXCEEDS_UPLOAD_LIMIT', 'tooBig').intercept((err)=>new Error('The photo upload failed: '+util.inspect(err)));

      //Detect if exist profile pic attached
      if(!UploadPathPic) {
        throw 'noFileAttached';
      }

      //settings the relative path 10.9.45.149
      let baseURL = `http://${ip}:1337/images/profile/portrait/`;
      const relative_path = path.relative(`.tmp/public/images/profile/portrait/`,`${UploadPathPic.fd}/`).replace(/\\/g, `${baseURL}`);

      sails.log.debug(baseURL + relative_path);

      //define path
      let urlPaths = baseURL + relative_path;

      //Update profile pic
      const UpdatePortraitPic = await users.update({id:req.params.id}).set({
        path_portrait:urlPaths
      }).fetch();

      //return data aftåer update portrait pic
      return res.ok(UpdatePortraitPic);

    } catch (err) {
      if(err.name === "ValidateError") {
        return res.badRequest({err: 'Check your info and try again!'});
      } else {
        return res.serverError({err});
      }
    }
  },
};
