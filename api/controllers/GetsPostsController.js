/**
 * GetsPostsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  //Lists all posts made by users
  async GetsAllPosts(req, res) {
    try {
      //Empty array
      const list_posts = [];
      const list = [];

      //make database query
      const posts = await Post.find({
        userId:req.params.id
      }).populate('PostDetails');

      //insert all posts in empty array
      list_posts.push(posts);

      //detect if array content data
      if (list_posts.length === "") {
        return res.serverError({
          error:"Oops do not exist any post"
        })
      } else {

        //Loop
        for (let i=0; i<=list_posts.length - 1; i++) {
          for (let j=0; j <= list_posts[i].length - 1; j++) {

            //creating objects
            const info = {
              id_post:list_posts[i][j]["id"],
              details:list_posts[i][j]["PostDetails"]
            };

            //show in console
            console.log(info);

            //insert object in array
            list.push(info);
          }

          //return json objects
          return res.json(list);
        }
      }
    } catch (err) {
      if(err.name === "ValidateError") {
        return res.badRequest({
          err: 'Post not found'
        })
      } else {
        return res.serverError({
          err:'Error, try again!'
        })
      }
    }
  }
};

