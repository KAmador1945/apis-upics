module.exports = {
  async findProfile(req, res) {
    try {

      //creating empty array
      const containerInfoUsr = [];

      //query to database
      const results = await users.find({
        id:req.params.id
      });

      //detect is exist any data
      if (results.length === "") {
        return res.serverError({
          err:"Info not found"
        })
      } else {

        //loop
        for (let i = 0; i <= results.length - 1; i++) {

          //creating object
          const info = {
            "id":results[i]["id"],
            "username":results[i]["username"],
            "first_name":results[i]["first_name"],
            "last_name":results[i]["last_name"],
            "path_pic":results[i]["path_pic"],
            "path_portrait":results[i]["path_portrait"],
            "bio":results[i]["bio"]
          };

          //insert every object in array
          containerInfoUsr.push(info);
        }

        sails.log.debug(containerInfoUsr);

        //returning data
        return res.json(containerInfoUsr);
      }
    } catch (err) {
      if (err) {
        return res.serverError(err);
      }
    }
  }
};
