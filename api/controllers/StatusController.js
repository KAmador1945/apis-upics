/**
 * StatusController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  //get user status
  async GetStatus(req, res) {
    try {

      //empty array
      const list = [];

      //making query to db
      const status = await users.find({
        id:req.params.id
      });

      if (status.length === "") {
        return res.serverError({
          err:'Status not found'
        })
      } else {
        //loops
        for (let i = 0; i <= status.length - 1; i++) {
          sails.log.debug(status[i]);

          //creating objects
          const status_usr = {
            "status":status[i]["status"]
          };

          //insert object in array
          list.push(status_usr);
        }
      }

      //returning result
      return res.json(list);

    } catch (err) {
      if(err.name == "ValidateError") {
        return res.badRequest({
          err:'User not found'
        })
      } else {
        return res.serverError({
          err:'Error, try again!'
        })
      }
    }
  }
};

