//controller for to update profile pic
let util = require('util');
let path = require('path');
let addr = require('ip');
let ip = addr.address();

//Create module
module.exports = {

  //Update portrait picture
  async UpdateProfilePic(req, res) {

    try {

      //upload profile pic
      let UploadProfilePic = await sails.uploadOne(req.file('file'), {
        dirname:require('path').resolve(`.tmp/public/images/profile/picture/`),
        maxBytes:3000000
      }).intercept('E_EXCEEDS_UPLOAD_LIMIT', 'tooBig').intercept((err)=>new Error('The photo upload failed: '+util.inspect(err)));

      //Detect if exist profile pic attached
      if(!UploadProfilePic) {
        throw 'noFileAttached';
      }

      sails.log.debug(`Prueba ================>${ip}`);

      //settings the relative path
      let baseURL =`http://${ip}:1337/images/profile/picture/`;
      const relative_path = path.relative(`.tmp/public/images/profile/picture/`,`${UploadProfilePic.fd}/`).replace(/\\/g, `${baseURL}`);

      //define path
      let urlPaths = baseURL + relative_path;

      sails.log.debug(`URL PATHS ==>${urlPaths}`);

      //Update profile pic
      const UpdateProfilePic = await users.update({id:req.params.id}).set({
        path_pic:urlPaths
      }).fetch();

      return res.ok(UpdateProfilePic);

    } catch (err) {
      if(err.name === "ValidateError") {
        return res.badRequest({err: 'Check your info and try again!'});
      } else {
        return res.serverError({err});
      }
    }
  },
};
