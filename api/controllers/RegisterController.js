const Joi = require('joi');

module.exports = {

  //define create account method
  sign_up: async function(req, res) {
    try {
      //define schema to insert on database
      const UsersSchemas = Joi.object().keys({
        username: Joi.string().required(),
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        email: Joi.string().required(),
        password: Joi.string().required(),
        path_pic: Joi.string(),
        path_portrait: Joi.string(),
        bio:Joi.string(),
        status:Joi.string()
      });

      //creating empty array
      const containerNewUsr = [];

      //validate info and store data on array
      const {
        username,
        first_name,
        last_name,
        email,
        password,
        path_pic,
        path_portrait,
        bio,
        status
      } = await Joi.validate(req.allParams(), UsersSchemas);

      //receive and encrypt password
      const encryptedPassword = await ServicesUtils.hashPassword(password);

      const username_s = "@"+username;
      const status_user = "ACTIVE";
      const pathPic = "";
      const pathPortrait = "";
      const bioUser = "Add bio";

      //create new users
      const results = await users.create({
        username:username_s,
        first_name,
        last_name,
        email,
        password: encryptedPassword,
        path_pic:pathPic,
        path_portrait: pathPortrait,
        bio:bioUser,
        status:status_user
      }).fetch();

      //detect if exist any data
      if (results.length === "") {
        return res.serverError({
          err:"Oops, account did not created"
        });
      } else {

        //create object
        const ObjectsResults = {
          "id":results.id,
          "username":results.username,
          "first_name":results.first_name,
          "last_name":results.last_name,
          "path_pic":results.path_pic,
          "path_portrait":results.path_portrait,
          "bio":results.bio
        };

        //showing in console
        sails.log.debug(ObjectsResults);

        //returning data
        return res.send(ObjectsResults);
      }

      //catch some errors
    } catch (err) {
      if (err.name === "ValidateError") {
        return res.badRequest({err: 'Check your info and try again!'});
      } else {
        return res.serverError({err:"Error, try again!"});
      }
    }
  },
};
