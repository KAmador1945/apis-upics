/**
 * PostController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

let util = require('util');
let path = require('path');
let addr = require('ip');
let ip = addr.address();

module.exports = {

  /**
   * `PostController.create()`
   */
  //request file to upload
  create: async function (req, res) {

    //capture data to save
    let {caption, imageWidth, imageHeight} = req.allParams();

    sails.log.debug(`Encontre esto ===> ${ip}`);

    //showing debug in console of sails
    sails.log.debug(caption);
    sails.log.debug(imageWidth);
    sails.log.debug(imageHeight);


    // Upload photo
    let uploadPic = await sails.uploadOne(req.file('file'), {
      dirname:require('path').resolve(`.tmp/public/images/posts/post_by_usr/`),
      maxBytes: 3000000,
    }).intercept('E_EXCEEDS_UPLOAD_LIMIT', 'tooBig').intercept((err)=>new Error('The photo upload failed: '+util.inspect(err)));

    if(!uploadPic) {
      throw 'noFileAttached';
    }

    //settings the relative path
    let baseURL = `http://${ip}:1337/images/posts/post_by_usr/`;
    const relative_path = path.relative(`.tmp/public/images/posts/post_by_usr/`,`${uploadPic.fd}/`).replace(/\\/g,`${baseURL}`);

    sails.log.debug(baseURL + relative_path);

    //define the main path
    let urlPaths = baseURL + relative_path;

    //Create post details
    const details = await PostDetails.create({
      caption:caption,
      img_path: urlPaths,
      imageWidth:imageWidth,
      imageHeight:imageHeight,
    }).fetch();

    //create post
    const post = await Post.create({
      idUser: req.params.id,
      PostDetails: details.id,
    }).fetch();

    return res.send(post);

  },

  /**
   * `PostController.find()`
   */
  find: async function (req, res) {
    try {

      //define empty array
      const list_posts = [];
      const lists = [];

      //Try to find all posts made by user
      const posts = await Post.find({userId:req.params.id}).populate('PostDetails');

      //insert posts in array
      list_posts.push(posts);

      //detect if exist any data
      if (list_posts.length === "") {
        return res.serverError({
          error:"Oops do not exist any post"
        })
      } else {
        //loop
        for (let i = 0; i <= list_posts.length - 1; i++) {
          for (let j = 0; j <= list_posts[i].length - 1; j++) {
            const info = {
              id_post:list_posts[i][j]["id"],
              details:list_posts[i][j]["PostDetails"]
            };

            //show console
            console.log(info);

            //insert object in array
            lists.push(info);
          }
        }

        return res.send(lists);
      }

    } catch(error) {
      return res.serverError(error);
    }
  },
};



