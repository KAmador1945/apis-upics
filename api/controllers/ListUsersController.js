module.exports = {

  //List all users registered in Upics
  async ListAllUsers(req, res) {
    try {

      //Query to users collections
      const containerUsers = [];
      const ListUsers = await users.find();

      //detect if exist info
      if (ListUsers.length === "") {
        return res.serverError({
          err: "Users not found"
        });
      } else {

        //load all users
        for (let i=0; i <= ListUsers.length - 1 ; i++) {

          //object to show
          const info = {
            id:ListUsers[i]["id"],
            username:ListUsers[i]["username"],
            first_name:ListUsers[i]["first_name"],
            last_name:ListUsers[i]["last_name"],
            path_pic:ListUsers[i]["path_pic"],
            path_portrait:ListUsers[i]["path_portrait"],
            bio:ListUsers[i]["bio"],
          };

          //insert elements to array
          containerUsers.push(info);
        }

        console.log(containerUsers);

        return res.json(containerUsers);
      }

    } catch (err) {
      if (err.name === "ValidateError") {
        return res.badRequest({err: 'Not found users!'});
      } else {
        return res.serverError({err:"Error, try again!"});
      }
    }
  }
};
