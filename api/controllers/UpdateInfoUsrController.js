module.exports = {
  async UpdateInfo(req,res) {
    try {

      //gets data send from iOS
      let {username, first_name, last_name, bio} = req.allParams();

      //show debug
      sails.log.debug('Ecnontre esto ==>' + username);
      sails.log.debug('Ecnontre esto ==>' + first_name);
      sails.log.debug('Ecnontre esto ==>' + last_name);
      sails.log.debug('Ecnontre esto ==>' + bio);

      //update method
      await users.update({id:req.params.id}).set({
        username:username,
        first_name:first_name,
        last_name:last_name,
        bio:bio
      });

      //return json objects
      const response = {
        "username":username,
        "first_name":first_name,
        "last_name":last_name,
        "bio":bio
      };

      //return objects
      return res.json(response);

    } catch (err) {
      if (err.name === "validate erros") {
        return res.badRequest({err:"Check your info and try again"});
      } else {
        return res.serverError({err});
      }
    }
  }
};
