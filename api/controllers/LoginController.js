const Joi = require('joi');

module.exports = {

  //login function
  login: async function(req, res) {

    //define objects
    try {
      const schema = Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required()
      });

      //define params for to find user
      const {
        username,
        password
      } = await Joi.validate(req.allParams(), schema);

      //capture value
      const usernames = "@"+username;

      //debugin
      console.log(usernames);

      //query to database
      const dataUser = await users.findOne({
        username:usernames,
      });

      //Detect if user exist
      if (!dataUser || !password) {
        return res.serverError({err: 'User does not exist'});
      }

      //Compare password with credential on saved on database
      const matchPassword = await ServicesUtils.compareHash(password, dataUser.password);

      //Detect if password does not coinciden xd
      if(!matchPassword) {
        return res.serverError({err:'Password does not match'})
      }

      //Show in console status of user
      console.log(`Prueba de datos encontrados ===> ${dataUser["status"]}`);

      //Detect if account was suspended
      if (dataUser["status"] !== "ACTIVE") {
        return res.serverError({
          err:'This account was suspended',
        })
      } else {

        //Create objects
        const ObjectsResults = {
          "id":dataUser.id,
          "username":usernames,
          "first_name":dataUser.first_name,
          "last_name":dataUser.last_name,
          "path_pic":dataUser.path_pic,
          "path_portrait":dataUser.path_portrait,
          "bio":dataUser.bio,
          "status":dataUser.status,
        };

        //show in console the data to response
        console.log(ObjectsResults);

        //return all value
        return res.json(ObjectsResults);
      }

    } catch (err) {
      res.serverError({err:"Error, try again!"});
    }
  },
};
