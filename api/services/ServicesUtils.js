const bcrypt = require('bcrypt');
const salt_rounds = 10;

module.exports = {

  //Encrypt the password
  async hashPassword(password) {
    return await bcrypt.hash(password, salt_rounds);
  },

  //Compare password for to start session
  async compareHash(password, hash) {
    return await bcrypt.compare(password, hash);
  }
};
