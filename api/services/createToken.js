'use strict';

const jwt = require('jsonwebtoken');
const secret = 'LeibstandarteSS1945';

module.exports = {

  //create token of every user
  IsUser(payload, experiresIn) {
    return jwt.sign(payload, secret, {
      experiresIn
    });
  },

  //verify token
  Verify(token) {
    return jwt.verify(token, secret);
  }
};
