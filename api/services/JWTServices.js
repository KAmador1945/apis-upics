const jwt = require("jsonwebtoken");
const secret = '312341245';
module.exports = {

  //Detect if is a user
  IsUser(payload, expiresIn) {
    return jwt.sign(payload, secret, {
      expiresIn
    });
  },

  //Verify tokens
  verify(token) {
    return jwt.verify(token, secret);
  }
};
