/**
 * PostDetails.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    caption: {
      type: 'string',
      required:true
    },

    img_path: {
      type: 'string' ,
      required:true
    },

    imageWidth: {
      type:"number",
      required: true
    },

    imageHeight: {
      type:"number",
      required:true
    },
  },

};

