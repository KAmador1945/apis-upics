module.exports = {
  attribute: {
    username: {
      type: 'string',
      unique: true,
      required: true
    },
    first_name: {
      type: 'string',
      required: true
    },
    last_name: {
      type: 'string',
      required: true
    },
    email: {
      type: 'string',
      isEmail: true,
      required: true,
      unique: true,
      lowercase: true
    },
    password: {
      type: 'string',
      required: true,
      minLength: 5
    },
    path_pic: {
      type:'string',
      required:false
    },
    path_portrait: {
      type:'string',
      required: false
    },
    bio: {
      type:'string',
      require: false
    },
    status: {
      type:'string',
      required:false
    }
  },
};
