/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

//Define the routes of APIs
module.exports.routes = {

  //define the url for register
  'POST /sign_up':'RegisterController.sign_up',

  //define url for log in
  'POST /sign_in':'LoginController.login',

  //Upload profile pic
  'POST /update_profile_pic/:id':'UpdateProfilePicController.UpdateProfilePic',

  //Update portrait pic
 'POST /update_portrait/:id':'UpdatePortraitPicController.UpdatePortrait',

  //Update info user
  'PATCH /update_info/:id':'UpdateInfoUsrController.UpdateInfo',

  //Get info user
  'GET /find/:id':'InfoUserController.findProfile',

  //Create Post
  'POST /post/:id':'PostController.create',

  //Get posts made of user
  'GET /find_posts/:id':'PostController.find',

  //Get all users
  'GET /list_users/':'ListUsersController.ListAllUsers',

  //Get all posts
  'GET /feed_posts/:id':'GetsPostsController.GetsAllPosts',

  //Get status user
  'GET /status_usr/:id':'StatusController.GetStatus',
};
